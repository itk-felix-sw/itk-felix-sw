# ITK FELIX SW

This is a collection of software for FELIX read-out for ITK (DAQ and DCS),
that is mostly written in C++ with TDAQ bindings, and includes OKS database files for operation in SR1.
It is divided into several packages that provide different functionalities, based on the ATLAS TDAQ CMake project policy. 
TDAQ is required to compile and in some cases to run the software.

### Installation

1. Checkout itk-felix-sw package
```
git clone https://gitlab.cern.ch/itk-felix-sw/itk-felix-sw.git 
```

2. Change to the itk-felix-sw directory
```
cd itk-felix-sw
```

3. Checkout the packages that you need for your setup using the repo
```
repo init -u . -g RD53B  # Allowed flags are RD53B,Extras,TDAQ,BCM
repo sync
```

3. (Old Method)  Checkout the packages that you need for your setup using the repo
```
git clone https://gitlab.cern.ch/itk-felix-sw/RD53Emulator.git
git clone https://gitlab.cern.ch/itk-felix-sw/RD53AConfigs.git 
git clone https://gitlab.cern.ch/itk-felix-sw/Strips.git 



4. Setup the environment
```
source setup.sh
```

5. Make a build directory
```
mkdir build
```

6. Change to the build directory
```
cd build
```

7. Config the release for building
```
cmake ..
```
Important note here, if you want to use RD53B code with felix-star, instead of using the basic cmake command use this.
```
cmake ../ -DITKFELIXSTAR=true
```



8. Compile and install the software
```
make -j install
```

### Links

 * ATLAS TDAQ is available in the following [Link](https://gitlab.cern.ch/atlas-tdaq-software)
