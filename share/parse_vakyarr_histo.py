#!/usr/bin/env python
######################################################################
# Compare occupancy maps between itk-felix-sw and yarr
#
# Example: python compare_occ_maps.py 005357_lin_analogscan 002689_AnalogScanLin --chip_name CERNQ10_Chip2 
#
# @author Carlos.Solans@cern.ch
# @author Leyre.Flores.Sanz.De.Acedo@cern.ch
# @date October 2022
#######################################################################
import os
import sys
import math
import json
import ROOT

mem=[]

YARR_data_path = "/home/itkpix/YARRs/YARR_FELIX_MASTER/data/"
VAKYARR_data_path = "/home/itkpix/YARRs/YARR_gelix_devel/pyconfig/data/VakYARR_M6Demonstrator_-10degC/"

##Create a dictionary with the names of the Demonstrator M6PP0
M6PP0_demonstrator = {'CERNQ10_Chip2': ['M1_CERNQ10_Chip2','20UPGFC0013668'], 'CERNQ10_Chip4':['M1_CERNQ10_Chip4', '20UPGFC0013716'], 'Paris10_Chip2':['M2_Paris10_Chip2', '20UPGFC0008771'], 'Siegen1_Chip2':['M4_Siegen1_Chip2', '20UPGFC0012405'], 'Siegen1_Chip4':['M4_Siegen1_Chip4', '20UPGFC0012420'], 'Paris12_Chip2':['M5_Paris12_Chip2', '20UPGFC0012566'],'Siegen2_Chip2':['M6_Siegen2_Chip2', 'Siegen2_Chip_2'], 'Siegen2_Chip4':['M6_Siegen2_Chip4', 'Siegen2_Chip_4']}

##Definitions of the FE start and stop columns
SYN_COL0  = 0
SYN_COLN  = 128
LIN_COL0  = 128
LIN_COLN  = 264
DIFF_COL0 = 264
DIFF_COLN = 400
num_syn_pixels  = 24576
num_lin_pixels  = 25920
num_diff_pixels = 25920 

def parse(path,name):
    histo=None
    ext=os.path.splitext(path)[1]
    if "dat" in ext:
        cc=open(path).readlines()
        if "Histo1d" in cc[0]: 
            histo=parseHisto1d(cc)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in cc[0]:
            print("Parsing dat file: %s" % path)
            histo=parseHisto2d(cc)
            pass
        pass
    elif "json" in ext:
        try: j=json.load(open(path))
        except: return histo
        if "Histo1d" in j["Type"]:
            histo=jsonHisto1d(j,name)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in j["Type"]:
            print("Parsing histo2d json file: %s" % path)
            histo=jsonHisto2d(j,name)
            pass
        pass
    elif "root" in ext:
        print("Parsing root file: %s" % path)
        tf=ROOT.TFile(path)
        mem.append(tf)
        histo=tf.Get(name)
        histo.SetTitle("itk-felix-sw")
        histo.SetLineColor(ROOT.kBlue)
        histo.SetMarkerColor(ROOT.kBlue)
        pass
    return histo

def jsonHisto2d(lines, histo_name): 
    name  = histo_name
    title = "VAKYARR;%s;%s;%s" % (lines["Name"],lines["x"]["AxisTitle"],lines["y"]["AxisTitle"])
    xbins = int(lines["x"]["Bins"]) 
    xbin0 = float(lines["x"]["Low"])
    xbinN = float(lines["x"]["High"])
    ybins = int(lines["y"]["Bins"])
    ybin0 = float(lines["y"]["Low"])
    ybinN = float(lines["y"]["High"])
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    for i in range(0,xbins):
        sdata = lines["Data"][i]
        #print(len(sdata[0]))
        #print(sdata[0]) 
        x=i+1
        y=0
        for sv in sdata:
            y+=1
            histo.SetBinContent(x,y,float(sv))
            pass
        pass
    return histo    

def parseHisto2d(lines):
    if "Histo2d" not in lines[0]: return None
    name = lines[1].strip()
    title = "YARR;%s;%s;%s" % (lines[2].strip(),lines[3].strip(),lines[4].strip())
    xbins = int(lines[5].split()[0])
    xbin0 = float(lines[5].split()[1])
    xbinN = float(lines[5].split()[2])
    ybins = int(lines[6].split()[0])
    ybin0 = float(lines[6].split()[1])
    ybinN = float(lines[6].split()[2])
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    histo.Sumw2()
    for i in range(0,ybins):
        sdata = lines[8+i].strip().split()
        y=i+1
        x=0
        for sv in sdata:
            x+=1
            histo.SetBinContent(x,y,float(sv))
            #histo.SetBinError(x,y,math.sqrt(float(sv)))
            pass
        pass
    return histo

if __name__ == "__main__":
    import argparse

    parser=argparse.ArgumentParser("Compare YARR and ITK-FELIX-SW plots")
    parser.add_argument("file1", help = "Run folder of YARR data")
    parser.add_argument("file2", help = "Run folder from itk-felix-sw")
    parser.add_argument("--chip_name", help = "name of the chip to be compared")
    parser.add_argument("--fe", help = "Front-end flavour of the data")
  
    args=parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gROOT.ForceStyle()    
    ROOT.gStyle.SetPadRightMargin(0.2)

    if args.file1: 
        YARR_analysis_path = YARR_data_path +  args.file1
        print ("I am looking in the following folder: %s" %YARR_analysis_path )     
        if args.chip_name:
            dat_file = M6PP0_demonstrator[args.chip_name][1]+'_ThresholdMap-0.dat' 
            YARR_file = YARR_analysis_path + "/" + dat_file
            print ("The file to analyse is: %s" %YARR_file)

    if args.file2: 
        VAKYARR_analysis_path = VAKYARR_data_path + args.file2   
        print ("I am looking in the following file: %s" %VAKYARR_analysis_path)     
        if args.chip_name:
            json_file = M6PP0_demonstrator[args.chip_name][1]+'_ThresholdMap-0_0_0_350_0.json' 
            VAKYARR_file = VAKYARR_analysis_path + "/" + json_file
            print ("I am looking at the following json file: %s" %VAKYARR_file)
 
    if args.fe == "syn":
        start_bin_x = SYN_COL0
        stop_bin_x  = SYN_COLN
    elif args.fe == "lin": 
        start_bin_x = LIN_COL0
        stop_bin_x  = LIN_COLN
    elif args.fe == "diff": 
        start_bin_x = DIFF_COL0
        stop_bin_x  = DIFF_COLN
    elif args.fe == "lindiff": 
        start_bin_x = LIN_COL0
        stop_bin_x  = DIFF_COLN
    else:
        start_bin_x = 0
        stop_bin_x = 400

 
    histo1=parse(YARR_file, args.chip_name)

    histo2=parse(VAKYARR_file, args.chip_name)

    print("Total number of X bins with YARR is %d" %histo1.GetNbinsX()) 
    print("Total number of X bins with VAKYARR is %d" %histo2.GetNbinsX()) 
    print("Minimum value of X bin is %d and maximum is %d in YARR" %(histo1.GetXaxis().GetXmin(), histo1.GetXaxis().GetXmax())) 
    print("Minimum value of X bin is %d and maximum is %d in VAKYARR" %(histo2.GetXaxis().GetXmin(), histo2.GetXaxis().GetXmax()))


    c1=ROOT.TCanvas("c1","%s vs %s" % (histo1.GetTitle(), histo2.GetTitle()),1200,600)
    c1.Divide(2,1)
    c1.cd(1)
    histo1.Draw("colz")
    c1.cd(2)
    histo2.Draw("colz")
    c1.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/lin_thres_YARR_vs_VAKYARR/" + str(args.fe) + args.chip_name + "_2D_thres_map.pdf")
    #os.system("evince CERNQ10_occupancy_map.pdf &")
    
