#!/usr/bin/env python
######################################################################
# Analysis of data merging Compare occupancy maps between itk-felix-sw and yarr
#
# Example: python compare_occ_maps.py 005357_lin_analogscan 002689_AnalogScanLin --chip_name CERNQ10_Chip2 
#
# @author Carlos.Solans@cern.ch
# @author Leyre.Flores.Sanz.De.Acedo@cern.ch
# @date October 2022
#######################################################################
import os
import sys
import math
import json
import ROOT

mem=[]

ITk_flx_sw_data_path = "/home/itkpix/dataITkPix/"

#Create a list with the names of the data merging tests to analyse
data_merging_en_folders= ['003699_DigitalScan', '003700_DigitalScan', '003701_DigitalScan', '003702_DigitalScan', '003708_DigitalScan', '003704_DigitalScan', '003709_DigitalScan']

data_merging_injection_folders = ['003710_DigitalScan', '003711_DigitalScan', '003713_DigitalScan', '003712_DigitalScan', '003714_DigitalScan', '003707_DigitalScan']
occupancy=[100, 50, 40, 25, 5, 100]

data_merging_trim_folders = ['003689_DigitalScan', '003693_DigitalScan', '003691_DigitalScan', '003690_DigitalScan']

data_merging2to1 = ['003706_DigitalScan']

def parse(path,name):
    histo=None
    ext=os.path.splitext(path)[1]
    if "dat" in ext:
        cc=open(path).readlines()
        if "Histo1d" in cc[0]: 
            histo=parseHisto1d(cc)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in cc[0]:
            print("Parsing dat file: %s" % path)
            histo=parseHisto2d(cc)
            pass
        pass
    elif "json" in ext:
        try: j=json.load(open(path))
        except: return histo
        if "Histo1d" in j["Type"]:
            histo=jsonHisto1d(j,name)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in j["Type"]:
            print("Parsing json file: %s" % path)
            histo=jsonHisto2d(j,name)
            pass
        pass
    elif "root" in ext:
        print("Parsing root file: %s" % path)
        tf=ROOT.TFile(path)
        mem.append(tf)
        histo=tf.Get(name)
        print(type(histo))
        print(histo.ls())
        histo.SetTitle("itk-felix-sw")
        histo.SetLineColor(ROOT.kBlue)
        histo.SetMarkerColor(ROOT.kBlue)
        pass
    return histo

def parseHisto2d(lines):
    if "Histo2d" not in lines[0]: return None
    name = lines[1].strip()
    title = "YARR;%s;%s;%s" % (lines[2].strip(),lines[3].strip(),lines[4].strip())
    xbins = int(lines[5].split()[0])
    xbin0 = float(lines[5].split()[1])
    xbinN = float(lines[5].split()[2])
    ybins = int(lines[6].split()[0])
    ybin0 = float(lines[6].split()[1])
    ybinN = float(lines[6].split()[2])
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    histo.Sumw2()
    for i in range(0,ybins):
        sdata = lines[8+i].strip().split()
        y=i+1
        x=0
        for sv in sdata:
            x+=1
            histo.SetBinContent(x,y,float(sv))
            histo.SetBinError(x,y,math.sqrt(float(sv)))
            pass
        pass
    return histo

if __name__ == "__main__":
    import argparse

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gROOT.ForceStyle()    
    ROOT.gStyle.SetPadRightMargin(0.2)

    #Loop over the different folders and root files
    for i in data_merging_trim_folders:
        data_merging_en_analysis_path = ITk_flx_sw_data_path + i +"/output.root"
        print ("I am looking in the following file: %s" %data_merging_en_analysis_path) 
        root_files = ["occ_rd53b_quad_1","occ_rd53b_quad_2", "occ_rd53b_quad_3", "occ_rd53b_quad_4"]

        for j in root_files: 
            histo = parse(data_merging_en_analysis_path, j)
            print("Total number of X bins is %d" %histo.GetNbinsX() )  
            print("Total number of Y bins is %d" %histo.GetNbinsY())
            tot_number_pixels = 400 * 384
            print("Total number of pixel in the matrix is %d" %tot_number_pixels ) 
            print("Minimum value of X bin is %d and maximum is %d" %(histo.GetXaxis().GetXmin(), histo.GetXaxis().GetXmax()))
            
            #Analysis_histo_itk = ROOT.TH1D("hits", "; ; Pixels",5,-0.5,4.5)
            
            #Definition of analysis variables
            total_number_injections = 0
            noisy_pixel = 0
            death_pixel = 0
            perfect_occ = 0
            neither_nor = 0
            inneficient = 0 #Less than 99%
            good_enough = 0 #Betweem 99%-101%

            for y in range(1, histo.GetNbinsY()+1):
                for x in range(1, histo.GetNbinsX()+1):
                    z = histo.GetBinContent(x, y)
                    total_number_injections = total_number_injections + z
                    if (z == 100):
                        perfect_occ = perfect_occ + 1
                    elif (z == 0):     
                        death_pixel = death_pixel + 1
                    elif (z > 101):
                        noisy_pixel = noisy_pixel + 1
                    elif (z>=99 and z<=101):
                        good_enough = good_enough + 1
                    elif (z<=98 and z > 0):
                        inneficient = inneficient + 1
                    else:
                        neither_nor = neither_nor + 1


            print("Total number of perfect pixels is: %d (%2.2f %%) " %(perfect_occ,float(perfect_occ)/float(tot_number_pixels)*100) )
            print("Total number of noisy pixels is: %d (%2.2f %%) " %(noisy_pixel,float(noisy_pixel)/float(tot_number_pixels)*100) )
            print("Total number of death pixels is: %d (%2.2f %%) " %(death_pixel,float(death_pixel)/float(tot_number_pixels)*100) )
            print("Total number of inneficient pixels is: %d (%2.2f %%) " %(inneficient,float(inneficient)/float(tot_number_pixels)*100) )
            print("Total number of good enough pixels is: %d (%2.2f %%) " %(good_enough,float(good_enough)/float(tot_number_pixels)*100) )
            print("Total number of not good nor too bad pixels is: %d (%2.2f %%) " %(neither_nor,float(neither_nor)/float(tot_number_pixels)*100) )

            #Analysis_histo_itk.GetXaxis().SetBinLabel(1, "perfect_occ")
            #Analysis_histo_itk.Fill(1, perfect_occ)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(2, "noisy")
            #Analysis_histo_itk.Fill(2, noisy_pixel)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(3, "inneficient")
            #Analysis_histo_itk.SetBinContent(3, inneficient)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(4, "good enough")
            #Analysis_histo_itk.SetBinContent(4, good_enough)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(5, "death")
            #Analysis_histo_itk.Fill(5, death_pixel)
           
            print (" ")
            #c2=ROOT.TCanvas("c2", "Analysis merging occupancy", 0,0,1600, 800)
            #ROOT.gPad.SetRightMargin(0.2)
            #histo_sub.SetMaximum(5)
            #histo_sub.SetMinimum(-5)
            #Analysis_histo_itk.Draw()
            #c2.SaveAs("/home/itkpix/itk-felix-sw/share/" + i + j + ".pdf")
    
    trig_loop = 0
    for i in data_merging_injection_folders:
        trig_loop = trig_loop + 1
        data_merging_en_analysis_path = ITk_flx_sw_data_path + i +"/output.root"
        print ("I am looking in the following file: %s" %data_merging_en_analysis_path) 
        root_files = ["occ_rd53b_quad_1","occ_rd53b_quad_2", "occ_rd53b_quad_3", "occ_rd53b_quad_4"]

        for j in root_files: 
            histo = parse(data_merging_en_analysis_path, j)
            print("Total number of X bins is %d" %histo.GetNbinsX() )  
            print("Total number of Y bins is %d" %histo.GetNbinsY())
            tot_number_pixels = 400 * 384
            print("Total number of pixel in the matrix is %d" %tot_number_pixels ) 
            print("Minimum value of X bin is %d and maximum is %d" %(histo.GetXaxis().GetXmin(), histo.GetXaxis().GetXmax()))
            
            #Analysis_histo_itk = ROOT.TH1D("hits", "; ; Pixels",5,-0.5,4.5)
            
            #Definition of analysis variables
            total_number_injections = 0
            noisy_pixel = 0
            death_pixel = 0
            perfect_occ = 0
            neither_nor = 0
            inneficient = 0 #Less than 99%
            good_enough = 0 #Betweem 99%-101%
            
            for y in range(1, histo.GetNbinsY()+1):
                for x in range(1, histo.GetNbinsX()+1):
                    z = histo.GetBinContent(x, y)
                    total_number_injections = total_number_injections + z
                    if (z == occupancy[trig_loop - 1]):
                        perfect_occ = perfect_occ + 1
                    elif (z == 0):     
                        death_pixel = death_pixel + 1
                    elif (z > (occupancy[trig_loop -1] + 1 )):
                        noisy_pixel = noisy_pixel + 1
                    elif (z>= (occupancy[trig_loop -1 ] - 1) and z<= (occupancy[trig_loop - 1] + 1)):
                        good_enough = good_enough + 1
                    elif (z<= (occupancy[trig_loop -1] - 2) and z > 0):
                        inneficient = inneficient + 1
                    else:
                        neither_nor = neither_nor + 1


            print("Total number of perfect pixels is: %d (%2.2f %%) " %(perfect_occ,float(perfect_occ)/float(tot_number_pixels)*100) )
            print("Total number of noisy pixels is: %d (%2.2f %%) " %(noisy_pixel,float(noisy_pixel)/float(tot_number_pixels)*100) )
            print("Total number of death pixels is: %d (%2.2f %%) " %(death_pixel,float(death_pixel)/float(tot_number_pixels)*100) )
            print("Total number of inneficient pixels is: %d (%2.2f %%) " %(inneficient,float(inneficient)/float(tot_number_pixels)*100) )
            print("Total number of good enough pixels is: %d (%2.2f %%) " %(good_enough,float(good_enough)/float(tot_number_pixels)*100) )
            print("Total number of not good nor too bad pixels is: %d (%2.2f %%) " %(neither_nor,float(neither_nor)/float(tot_number_pixels)*100) )

            #Analysis_histo_itk.GetXaxis().SetBinLabel(1, "perfect_occ")
            #Analysis_histo_itk.Fill(1, perfect_occ)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(2, "noisy")
            #Analysis_histo_itk.Fill(2, noisy_pixel)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(3, "inneficient")
            #Analysis_histo_itk.SetBinContent(3, inneficient)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(4, "good enough")
            #Analysis_histo_itk.SetBinContent(4, good_enough)
            #Analysis_histo_itk.GetXaxis().SetBinLabel(5, "death")
            #Analysis_histo_itk.Fill(5, death_pixel)
           
            print (" ")

