#!/usr/bin/env python

import os
import argparse

if __name__=="__main__":
    
    parser=argparse.ArgumentParser()
    parser.add_argument('-v','--verbose',help='enable verbose mode',action='store_true')
    parser.add_argument('-d','--dst-dir',help='itk felix sw directory',default=os.environ['ITK_PATH'])
    parser.add_argument('-f','--flx-dir',help='Felix directory',required=True)
    args=parser.parse_args()

    libs=["libfelix-client-lib.so",
          "libfelix-client-thread.so",
          "libhdlc_coder.so",
          "libnetio-lib.so",
          "libfelix-client-lib.so",
          "libfelix-bus-lib.so",
          "libfelix-bus-server-lib.so",
          "libjwrite.so",
          "libsimdjson.so",
          "libfabric.so.1"]

    paths=[
        "",
        os.environ['CMTCONFIG']+"/felix-client",
        os.environ['CMTCONFIG']+"/lib",
    ]
    dst=args.dst_dir+"/installed/external/"+os.environ["CMTCONFIG"]+"/lib";
    if args.flx_dir[-1]=="/": args.flx_dir=args.flx_dir[:-1]

    if not os.path.exists(dst):
        print("Create destination directory: %s" % dst)
        os.system("mkdir -p %s" % dst);
        pass

    found={}
    for lib in libs:
        found[lib]=False
        for p in paths:
            if os.path.exists("%s/%s/%s" % (args.flx_dir,p,lib)):
                found[lib]=True
                os.system("cp -v %s/%s/%s %s/." % (args.flx_dir,p,lib,dst))
                pass
            pass
        pass

    for lib in found:
        if not found[lib]: print("Library not found: %s" % lib)
        pass

    print("Have a nice day")
