#!/usr/bin/env python
######################################################################
# Compare occupancy maps between itk-felix-sw and yarr
#
# Example: python compare_occ_maps.py 005357_lin_analogscan 002689_AnalogScanLin --chip_name CERNQ10_Chip2 
#
# @author Carlos.Solans@cern.ch
# @author Leyre.Flores.Sanz.De.Acedo@cern.ch
# @date October 2022
#######################################################################
import os
import sys
import math
import json
import ROOT

mem=[]

YARR_data_path = "/home/itkpix/YARRs/YARR_FELIX_MASTER/data/"
ITK_flx_sw_path = "/home/itkpix/data"

##Create a dictionary with the names of the Demonstrator modules M1-M6 is M6PP0 and M12-M22 is M12PP0 FE for ITk-felix-sw
Demonstrator_modules = {'CERNQ10_Chip2': ['M1_CERNQ10_Chip2','20UPGFC0013668'], 'CERNQ10_Chip4':['M1_CERNQ10_Chip4', '20UPGFC0013716'], 
                        'Paris10_Chip2':['M2_Paris10_Chip2', '20UPGFC0008771'], 
                        'Siegen1_Chip2':['M4_Siegen1_Chip2', '20UPGFC0012405'], 'Siegen1_Chip4':['M4_Siegen1_Chip4', '20UPGFC0012420'], 
                        'Paris12_Chip2':['M5_Paris12_Chip2', '20UPGFC0012566'],
                        'Siegen2_Chip2':['M6_Siegen2_Chip2', 'Siegen2_Chip_2'], 'Siegen2_Chip4':['M6_Siegen2_Chip4', 'Siegen2_Chip_4'], 
                        'KEKQ22':['M12_KEKQ22_Chip4','20UPGM20024010'], 
                        'KEKQ24':['M13_KEKQ24_Chip4','20UPGM20024014'], 
                        'KEKQ25':['M14_KEKQ25_Chip4','20UPGM20024001'], 
                        'Liv8':['M15_Liv8_Chip4','20UPGM20025058'], 
                        'KEKQ19':['M16_KEKQ19_Chip4','20UPGM20024004'], 
                        'Paris6':['M17_Paris6_Chip4','20UPGM20024132'], 
                        'Goe5':['M18_Goe5_Chip4','20UPGM20021055'], 
                        'Paris16':['M19_Paris16_Chip4','20UPGM20025047'], 
                        'Paris11':['M20_Paris11_Chip4','20UPGM20024022'], 
                        'Goe7':['M21_Goe7_Chip4','20UPGM20021159'], 
                        'Paris8':['M22_Paris8_Chip4','20UPGM20021171']
}

##Definitions of the FE start and stop columns
SYN_COL0  = 0
SYN_COLN  = 128
LIN_COL0  = 128
LIN_COLN  = 264
DIFF_COL0 = 264
DIFF_COLN = 400
num_syn_pixels  = 24576
num_lin_pixels  = 25920
num_diff_pixels = 25920 

def parse(path,name):
    histo=None
    ext=os.path.splitext(path)[1]
    if "dat" in ext:
        cc=open(path).readlines()
        if "Histo1d" in cc[0]: 
            histo=parseHisto1d(cc)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in cc[0]:
            print("Parsing dat file: %s" % path)
            histo=parseHisto2d(cc)
            pass
        pass
    elif "json" in ext:
        try: j=json.load(open(path))
        except: return histo
        if "Histo1d" in j["Type"]:
            histo=jsonHisto1d(j,name)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in j["Type"]:
            print("Parsing json file: %s" % path)
            histo=jsonHisto2d(j,name)
            pass
        pass
    elif "root" in ext:
        print("Parsing root file: %s" % path)
        tf=ROOT.TFile(path)
        mem.append(tf)
        histo=tf.Get(name)
        histo.SetTitle("itk-felix-sw")
        histo.SetLineColor(ROOT.kBlue)
        histo.SetMarkerColor(ROOT.kBlue)
        pass
    return histo

def parseHisto2d(lines):
    if "Histo2d" not in lines[0]: return None
    name = lines[1].strip()
    title = "YARR;%s;%s;%s" % (lines[2].strip(),lines[3].strip(),lines[4].strip())
    xbins = int(lines[5].split()[0])
    xbin0 = float(lines[5].split()[1])
    xbinN = float(lines[5].split()[2])
    ybins = int(lines[6].split()[0])
    ybin0 = float(lines[6].split()[1])
    ybinN = float(lines[6].split()[2])
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    histo.Sumw2()
    for i in range(0,ybins):
        sdata = lines[8+i].strip().split()
        y=i+1
        x=0
        for sv in sdata:
            x+=1
            histo.SetBinContent(x,y,float(sv))
            #histo.SetBinError(x,y,math.sqrt(float(sv)))
            pass
        pass
    return histo

if __name__ == "__main__":
    import argparse

    parser=argparse.ArgumentParser("Compare YARR and ITK-FELIX-SW plots")
    parser.add_argument("file1", help = "Run folder of YARR data")
    parser.add_argument("file2", help = "Run folder from itk-felix-sw")
    parser.add_argument("--chip_name", help = "name of the chip to be compared")
    parser.add_argument("--fe", help = "Front-end flavour of the data")
 
    args=parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gROOT.ForceStyle()    
    ROOT.gStyle.SetPadRightMargin(0.2)

    if args.file1:    
        YARR_analysis_path = YARR_data_path +  args.file1
        print ("I am looking in the following folder: %s" %YARR_analysis_path )     
        if args.chip_name:
            dat_file = Demonstrator_modules[args.chip_name][1]+'_ThresholdMap-0.dat' 
            YARR_file = YARR_analysis_path + "/" + dat_file
            print ("The file to analyse is: %s" %YARR_file)

    if args.file2: 
        itk_flx_sw_path = ITK_flx_sw_path + "/" + args.file2 + "/output.root"  
        print ("I am looking in the following file: %s" %itk_flx_sw_path )     
        if args.chip_name:
            root_file = "thres_Q_M6PP0_" + Demonstrator_modules[args.chip_name][0] 
            print ("I am looking at the following tree: %s" %root_file)
 
    if args.fe == "syn":
        start_bin_x = SYN_COL0
        stop_bin_x  = SYN_COLN
    elif args.fe == "lin": 
        start_bin_x = LIN_COL0
        stop_bin_x  = LIN_COLN
    elif args.fe == "diff": 
        start_bin_x = DIFF_COL0
        stop_bin_x  = DIFF_COLN
    elif args.fe == "lindiff": 
        start_bin_x = LIN_COL0
        stop_bin_x  = DIFF_COLN
    else:
        start_bin_x = 0
        stop_bin_x = 400

 
    histo1=parse(YARR_file, args.chip_name)
    histo2=parse(itk_flx_sw_path,root_file)
    print("Total number of X bins with YARR is %d" %histo1.GetNbinsX()) 
    print("Total number of X bins with ITK-flx-sw is %d" %histo2.GetNbinsX()) 
    print("Minimum value of X bin is %d and maximum is %d in YARR" %(histo1.GetXaxis().GetXmin(), histo1.GetXaxis().GetXmax())) 
    print("Minimum value of X bin is %d and maximum is %d in ITK-flx-sw" %(histo2.GetXaxis().GetXmin(), histo2.GetXaxis().GetXmax()))

    #Generating the histo that is the substraction of the other two
    histo_sub = histo2.Clone()
    #-1 is the factor histo 1 is multiplied by
    histo_sub.Add(histo1, -1)


    #Threshold 2D analysis histos
    Analysis_histo_thres_itk_sub_YARR = ROOT.TH1D("hits","; ; Thres(ITK-flx-sw - YARR)",80,-200,200)
    Correlation_thres_histo = ROOT.TH2D("Correlation", "Correlation Threshold YARR vs ITK-flx-sw", 100, 1300, 1700, 100, 1300, 1700)
    thres_line = ROOT.TLine(1300, 1300, 1700, 1700) 

    bad_thres_scan_YARR = []
    bad_thres_scan_itk = []


    #Loop over     
    for y in range(1, histo1.GetNbinsY()):
        for x in range(start_bin_x + 1, stop_bin_x):
             z_YARR = histo1.GetBinContent(x, y)
             if (z_YARR == 0):
                 bad_thres_scan_YARR.append(x)
                 bad_thres_scan_YARR.append(y)
             z_itk = histo2.GetBinContent(x, y)
             if (z_itk ==0):
                 bad_thres_scan_itk.append(x)
                 bad_thres_scan_itk.append(y)
             Correlation_thres_histo.Fill(z_itk,z_YARR,1)
             Analysis_histo_thres_itk_sub_YARR.Fill(z_itk - z_YARR)


    print("Pixels that couldn't get the threshold scan done with YARR are:")
    print(bad_thres_scan_YARR)
    print("Pixels that couldn't get the threshold scan done with itk-flx-sw are:")
    print(bad_thres_scan_itk)


    c1=ROOT.TCanvas("c1","%s vs %s" % (histo1.GetTitle(), histo2.GetTitle()),1200,600)
    c1.Divide(2,1)
    c1.cd(1)
    histo1.Draw("colz")
    c1.cd(2)
    histo2.Draw("colz")
    c1.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/lin_thres/" + str(args.fe) + args.chip_name + "_2D_thres_map.pdf")
    #os.system("evince CERNQ10_occupancy_map.pdf &")
    
    c2=ROOT.TCanvas("c2", "Difference in occupancy", 0,0,1600, 800)
    ROOT.gPad.SetRightMargin(0.2)
    histo_sub.SetMaximum(200)
    histo_sub.SetMinimum(-200)
    histo_sub.Draw("colz")
    c2.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/lin_thres/" + str(args.fe) + args.chip_name + "_difference_thres_YARR_vs_itk-flx-sw.pdf")
    #os.system("evince Difference_occ_YARR_vs_itk-flx-sw.pdf &")

    c=ROOT.TCanvas("canvas", "Analysis histo", 1200, 600)
    #ROOT.gStyle.SetEndErrorSize(3)
    #ROOT.gStyle.SetErrorX(1.)
    Analysis_histo_thres_itk_sub_YARR.SetMarkerStyle(20)
    #Analysis_histo_itk_sub_YARR.SetMarkerStyle(24)
    Analysis_histo_thres_itk_sub_YARR.Draw("HIST")
    #Analysis_histo_itk.Draw("ESAME")
    #Analysis_histo_YARR.SetFillColor(49)
    #Analysis_histo_itk.SetFillColor(38)
    legend=ROOT.TLegend(0.55,0.65,0.76,0.82)
    #legend.AddEntry(Analysis_histo_YARR, "ITK_FELIX_SW - YARR", "f") 
    #legend.AddEntry(Analysis_histo_YARR, "YARR", "f")    
    #legend.AddEntry(Analysis_histo_itk, "ITk-FLX_SW", "f")    
    #legend.Draw()
    c.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/lin_thres/" + str(args.fe) + args.chip_name + "_analysis_thres_YARR_vs_ITk-flx-sw.pdf")

    c3=ROOT.TCanvas("c3", "Correlation ITK-flx-sw vs YARR threshold", 1600, 800)
    ROOT.gPad.SetRightMargin(0.2)
    Correlation_thres_histo.Draw("colz")
    thres_line.SetLineColor(632)
    thres_line.SetLineWidth(8)
    thres_line.Draw("same")
    Correlation_thres_histo.SetTitle("Threshold correlation itk-flx-sw vs YARR" + args.chip_name) 
    Correlation_thres_histo.GetXaxis().SetTitle("Threshold ITK-flx-sw (e-)")
    Correlation_thres_histo.GetYaxis().SetTitle("Threshold YARR (e-)")
    c3.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/lin_thres/" + str(args.fe) + args.chip_name + "correlation_threshold.pdf")
