#!/usr/bin/env python
######################################################################
# Compare plots between itk-felix-sw and yarr
#
# Example: python -i compare_scan_plots.py \
# /home/itkpix/YARRs/YARR_FELIX_MASTER/data/003314_lin_thresholdscan/JohnDoe_0_ThresholdDist-0.dat 
# /home/itkpix/data/001525_ThrScan/output.root --name2 thres_Q_1d_rd53a-1 --overlay
#
# @author Carlos.Solans@cern.ch
# @date August 2022
#######################################################################
import os
import sys
import math
import json
import ROOT

mem=[]

def parseHisto1d(lines):
    if "Histo1d" not in lines[0]: return None
    name = lines[1].strip()
    title = "YARR;%s;%s" % (lines[2].strip(),lines[3].strip())
    nbins = int(lines[5].split()[0])
    xbin0 = float(lines[5].split()[1])
    xbinN = float(lines[5].split()[2])
    sdata = lines[7].strip().split()
    histo = ROOT.TH1D(name,title,nbins,xbin0,xbinN)
    histo.Sumw2()
    count=0
    for sv in sdata:
        count+=1
        histo.SetBinContent(count,float(sv))
        histo.SetBinError(count,math.sqrt(float(sv)))
        pass
    pass
    return histo

def parseHisto2d(lines):
    if "Histo2d" not in lines[0]: return None
    name = lines[1].strip()
    title = "YARR;%s;%s;%s" % (lines[2].strip(),lines[3].strip(),lines[4].strip())
    xbins = int(lines[5].split()[0])
    xbin0 = float(lines[5].split()[1])
    xbinN = float(lines[5].split()[2])
    ybins = int(lines[6].split()[0])
    ybin0 = float(lines[6].split()[1])
    ybinN = float(lines[6].split()[2])
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    histo.Sumw2()
    for i in range(0,ybins):
        sdata = lines[8+i].strip().split()
        y=i+1
        x=0
        for sv in sdata:
            x+=1
            histo.SetBinContent(x,y,float(sv))
            histo.SetBinError(x,y,math.sqrt(float(sv)))
            pass
        pass
    return histo

def jsonHisto1d(j,name):
    if not j: return None
    name = j["Name"]
    title = "YARR;%s;%s" % (j["x"]["AxisTitle"],j["y"]["AxisTitle"])
    xbins = j["x"]["Bins"]
    xbin0 = j["x"]["Low"]
    xbinN = j["x"]["High"]
    histo = ROOT.TH1F(name,title,xbins,xbin0,xbinN)
    histo.Sumw2()
    x=0
    for point in j["Data"]:
        x+=1
        histo.SetBinContent(x,point)
        histo.SetBinError(x,math.sqrt(point))
        pass
    return histo

def jsonHisto2d(j,name):
    if not j: return None
    name = j["Name"] if not name else name
    title = "YARR;%s;%s;%s" % (j["x"]["AxisTitle"],j["y"]["AxisTitle"],j["z"]["AxisTitle"])
    xbins = j["x"]["Bins"]
    xbin0 = j["x"]["Low"]
    xbinN = j["x"]["High"]
    ybins = j["y"]["Bins"]
    ybin0 = j["y"]["Low"]
    ybinN = j["y"]["High"]
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    histo.Sumw2()
    x=0
    for row in j["Data"]:
        y=0
        x+=1
        for point in row:
            y+=1
            histo.SetBinContent(x,y,point)
            histo.SetBinError(x,y,math.sqrt(point))
            pass
        pass
    return histo

def parse(path,name):
    histo=None
    ext=os.path.splitext(path)[1]
    if "dat" in ext:
        cc=open(path).readlines()
        if "Histo1d" in cc[0]: 
            histo=parseHisto1d(cc)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in cc[0]: 
            histo=parseHisto2d(cc)
            pass
        pass
    elif "json" in ext:
        try: j=json.load(open(path))
        except: return histo
        if "Histo1d" in j["Type"]:
            histo=jsonHisto1d(j,name)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in j["Type"]:
            histo=jsonHisto2d(j,name)
            pass
        pass
    elif "root" in ext:
        print("Parsing root file: %s" % path)
        tf=ROOT.TFile(path)
        mem.append(tf)
        histo=tf.Get(name)
        histo.SetTitle("itk-felix-sw")
        histo.SetLineColor(ROOT.kBlue)
        histo.SetMarkerColor(ROOT.kBlue)
        pass
    return histo

def rebin(histo1,histo2):
    bw1=int(histo1.GetXaxis().GetBinWidth(1))
    bw2=int(histo2.GetXaxis().GetBinWidth(1))
    lcm = math.lcm(bw1,bw2)
    print("histo1 bw:%i"%bw1)
    print("histo2 bw:%i"%bw2)
    print("lcm:%i"%lcm)
    h1=histo1.Clone()
    h2=histo2.Clone()
    h1.Rebin(int(lcm/bw1))
    h2.Rebin(int(lcm/bw2))
    return (h1,h2)

    
if __name__ == "__main__":

    import argparse

    parser=argparse.ArgumentParser("Compare YARR and ITK-FELIX-SW plots")
    parser.add_argument("file1")
    parser.add_argument("file2")
    parser.add_argument("-n1","--name1", help="name of histogram in file1 if is a root file")
    parser.add_argument("-n2","--name2", help="name of histogram in file2 if is a root file")
    parser.add_argument("-t","--opts", help="draw options", default="HISTE")
    parser.add_argument("--overlay",help="overlay the histograms",action="store_true")
    parser.add_argument("-o","--output",help="output file")
    parser.add_argument("-o2","--output2",help="output file2")
    parser.add_argument("-b","--batch",help="enable batch mode",action="store_true")
    parser.add_argument("-n","--norm",help="normalize to unity",action="store_true")

    args=parser.parse_args()

    if args.batch: ROOT.gROOT.SetBatch(True)

    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gROOT.ForceStyle()

    histo1=parse(args.file1,args.name1)
    histo2=parse(args.file2,args.name2)

    c1=ROOT.TCanvas("c1","%s vs %s" % (histo1.GetTitle(), histo2.GetTitle()),1200,600)
    c1.Divide(2,1)
    c1.cd(1)
    histo1.Draw(args.opts)
    c1.cd(2)
    histo2.Draw(args.opts)

    if args.output:
        c1.SaveAs(args.output)
        pass
        
    if args.overlay:
        c2=ROOT.TCanvas("c2","Overlay",800,600)
        #h1,h2=rebin(histo1,histo2)
        int1=int(histo1.Integral(0,-1))
        int2=int(histo2.Integral(0,-1))
        if args.norm:
            histo1.Scale(1/histo1.Integral())
            histo2.Scale(1/histo2.Integral())
            pass
        histo1.Scale(1/histo1.GetXaxis().GetBinWidth(1))
        histo2.Scale(1/histo2.GetXaxis().GetBinWidth(1))

        hs=ROOT.THStack()
        hs.SetTitle(";%s;%s"%(histo1.GetXaxis().GetTitle(),histo1.GetYaxis().GetTitle()))
        hs.Add(histo1)
        hs.Add(histo2)
        hs.Draw("NOSTACKHISTE")
        lg=ROOT.TLegend(0.75,0.8,0.99,0.95)
        lg.AddEntry(histo1,(histo1.GetTitle()+": "+str(int1)),"l")
        lg.AddEntry(histo2,(histo2.GetTitle()+": "+str(int2)),"l")
        lg.Draw()
        c2.Update()
        if args.output2:
            c2.SaveAs(args.output2)
            pass
        pass
    
    if not args.batch: input("press any key")
