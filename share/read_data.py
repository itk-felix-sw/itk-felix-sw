#!/usr/bin/env python
######################################################
# Simple tool to read binary data from TDAQ files
#
# Carlos.Solans@cern.ch
# Oliver.Thielmann@cern.ch
# June 2020
######################################################
import os
import sys
import struct
import argparse
import builtins

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",action="store_true")
parser.add_argument("-f","--file",required=True)
parser.add_argument("-i","--interactive",action="store_true")
args=parser.parse_args()

with open(args.file, mode='rb') as file: 
    header= False
    evnum=0
    while True:
        rbyte = file.read(4)
        if not rbyte: break
        bytes=rbyte[3]+rbyte[2]+rbyte[1]+rbyte[0]
        hstr=bytes.encode("hex")
        bstr="{0:032b}".format(struct.unpack('I',rbyte)[0])
        if(args.interactive and hstr=="aa1234aa"):
            builtins.input("Found event: %i. Press any key to continue"%evnum)
            evnum+=1
            pass
        print("%s     =     %s" % (bstr,hstr))
        pass
    pass

print ("Have a nice day")


