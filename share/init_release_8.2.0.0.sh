#!/bin/bash
#run this script like this
#./init_release-xxx.sh &> out.log &

RELEASE=7.1.0.0
TDAQ_RELEASE=tdaq-07-01-00
EXT_RELEASE=20.7.5

TILERELEASE_VERSION=v0r0p33

TILE_RELEASE=tile-$RELEASE
INST_RELEASE=installed-$RELEASE

TILE_PATH=/afs/cern.ch/user/t/tiledaq/public/tilercd
EXT_PATH=/afs/cern.ch/user/t/tiledaq/public/external
INST_PATH=/afs/cern.ch/user/t/tiledaq/scratch0

#CMTCONFIGS=(x86_64-slc6-gcc49-opt)

TILE_GITROOT=https://:@gitlab.cern.ch:8443/atlas-tile-online



do_InstallArea(){
(
    mkdir -p $TILE_PATH/$TILE_RELEASE
    mkdir -p $INST_PATH/$INST_RELEASE

    ln -snf $EXT_PATH/$EXT_RELEASE/InstallArea $TILE_PATH/$TILE_RELEASE/external
    ln -snf $EXT_PATH/$EXT_RELEASE/InstallArea $INST_PATH/external-$RELEASE
    ln -snf $INST_PATH/$INST_RELEASE $TILE_PATH/$TILE_RELEASE/installed

)
}




do_checkout(){
(
    source /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh ${TDAQ_RELEASE}
	 export GITROOT=${TILE_GITROOT}

    #define environment
    export TILE_INST_PATH=$TILE_PATH/$TILE_RELEASE
	 cd ${TILE_INST_PATH} 
    git clone $GITROOT/TileVMEboards.git
)
}

do_compile(){
(
    
	

    #define environment
    export TILE_INST_PATH=$TILE_PATH/$TILE_RELEASE/installed
    export TILE_EXT_PATH=$TILE_PATH/$TILE_RELEASE/external

    #compile TileRelease
    cd ${TILE_PATH}/${TILE_RELEASE}
	 #do config
	 source /afs/cern.ch/atlas/project/tdaq/cmake/projects/tdaq/${TDAQ_RELEASE}/installed/setup.sh
    cmake_config && cd $CMTCONFIG && make -j 8 && make install/fast
)
}


echo "Compiling Tile release"

echo "Build InstallArea"
do_InstallArea

echo "Check out"
do_checkout

echo "Compile"
do_compile

echo "Release has been built"