#!/usr/bin/env python
import json


def merge_4x1_v24(cfg1, cfg2, cfg3, cfg4):
  
  #Open the config file of FE1 in the module
  with open (cfg1) as f1_merge:
     chip1=json.load(f1_merge)
  #Access and change the value
  chip1["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip1["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip1["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip1["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip1["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip1["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerEnLane"]= 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  #Write the updated values in the file
  with open (cfg1, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE1 for flex v2.4")
     json.dump(chip1, outfile, sort_keys = True, indent = 4) 
  f1_merge.close()

  #Open the config file of FE1 in the module
  with open (cfg2) as f2_merge:
     chip2=json.load(f2_merge)
  #Access and change the value
  chip2["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip2["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip2["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip2["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip2["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip2["RD53B"]["GlobalConfig"]["SerEnLane"] = 4
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  #Write the updated values in the file
  with open (cfg2, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE2 for flex v2.4")
     json.dump(chip2, outfile, sort_keys = True, indent = 4) 
  f2_merge.close()

  #Open the config file of FE3 in the module
  with open (cfg3) as f3_merge:
     chip3=json.load(f3_merge)
  #Access and change the value
  chip3["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip3["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip3["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip3["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip3["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip3["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerEnLane"] = 4
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  #Write the updated values in the file
  with open (cfg3, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE3 for flex v2.4")
     json.dump(chip3, outfile, sort_keys = True, indent = 4) 
  f3_merge.close()

  #Open the config file of FE4 in the module
  with open (cfg4) as f4_merge:
     chip4=json.load(f4_merge)
  #Access and change the value
  chip4["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip4["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip4["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip4["RD53B"]["GlobalConfig"]["CdrClkSel"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip4["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip4["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeEn"] = 7
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip4["RD53B"]["GlobalConfig"]["SerEnLane"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux0"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux1"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux2"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux3"] = 3

  #Write the updated values in the file
  with open (cfg4, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE4 for flex v2.4")
     json.dump(chip4, outfile, sort_keys = True, indent = 4) 
  f4_merge.close()


################################################################

def merge_4x1_v32(cfg1, cfg2, cfg3, cfg4):
  
  #Open the config file of FE1 in the module
  with open (cfg1) as f1_merge:
     chip1=json.load(f1_merge)
  #Access and change the value
  chip1["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias0"] = 400
  chip1["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip1["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip1["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip1["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip1["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerEnLane"]= 6
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 1
  #Write the updated values in the file
  with open (cfg1, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE1 for flex v3.2")
     json.dump(chip1, outfile, sort_keys = True, indent = 4) 
  f1_merge.close()

  #Open the config file of FE1 in the module
  with open (cfg2) as f2_merge:
     chip2=json.load(f2_merge)
  #Access and change the value
  chip2["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias0"] = 400
  chip2["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip2["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip2["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip2["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip2["RD53B"]["GlobalConfig"]["SerEnLane"] = 5
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 3
  #Write the updated values in the file
  with open (cfg2, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE2 for flex v3.2")
     json.dump(chip2, outfile, sort_keys = True, indent = 4) 
  f2_merge.close()

  #Open the config file of FE3 in the module
  with open (cfg3) as f3_merge:
     chip3=json.load(f3_merge)
  #Access and change the value
  chip3["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias0"] = 300
  chip3["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip3["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip3["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip3["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip3["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerEnLane"] = 12
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 1
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 2
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  #Write the updated values in the file
  with open (cfg3, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE3 for flex v3.2")
     json.dump(chip3, outfile, sort_keys = True, indent = 4) 
  f3_merge.close()

  #Open the config file of FE4 in the module
  with open (cfg4) as f4_merge:
     chip4=json.load(f4_merge)
  #Access and change the value
  chip4["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip4["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip4["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip4["RD53B"]["GlobalConfig"]["CdrClkSel"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip4["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip4["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeEn"] = 13
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip4["RD53B"]["GlobalConfig"]["SerEnLane"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 3
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux0"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux1"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux2"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux3"] = 3

  #Write the updated values in the file
  with open (cfg4, 'w') as outfile:
     print("Dumping merging configuration 4 to 1 of FE4 for flex v3.2")
     json.dump(chip4, outfile, sort_keys = True, indent = 4) 
  f4_merge.close()



################################################################

def merge_2x1_v24(cfg1, cfg2, cfg3, cfg4):
  
  #Open the config file of FE1 in the module
  with open (cfg1) as f1_merge:
     chip1=json.load(f1_merge)
  #Access and change the value
  chip1["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip1["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip1["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip1["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip1["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 3
  chip1["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerEnLane"]= 3
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  #Write the updated values in the file
  with open (cfg1, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE1 flex v2.4")
     json.dump(chip1, outfile, sort_keys = True, indent = 4) 
  f1_merge.close()

  #Open the config file of FE1 in the module
  with open (cfg2) as f2_merge:
     chip2=json.load(f2_merge)
  #Access and change the value
  chip2["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip2["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip2["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["CdrClkSel"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip2["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip2["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 1
  chip2["RD53B"]["GlobalConfig"]["SerEnLane"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeInMux0"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeInMux1"] = 0
  #Write the updated values in the file
  with open (cfg2, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE2 flex v2.4")
     json.dump(chip2, outfile, sort_keys = True, indent = 4) 
  f2_merge.close()

  #Open the config file of FE3 in the module
  with open (cfg3) as f3_merge:
     chip3=json.load(f3_merge)
  #Access and change the value
  chip3["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip3["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip3["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip3["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip3["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 3
  chip3["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerEnLane"] = 3
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  #Write the updated values in the file
  with open (cfg3, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE3 flex v2.4")
     json.dump(chip3, outfile, sort_keys = True, indent = 4) 
  f3_merge.close()

  #Open the config file of FE4 in the module
  with open (cfg4) as f4_merge:
     chip4=json.load(f4_merge)
  #Access and change the value
  chip4["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip4["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip4["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip4["RD53B"]["GlobalConfig"]["CdrClkSel"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip4["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip4["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 1
  chip4["RD53B"]["GlobalConfig"]["SerEnLane"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux0"] = 3
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux1"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux2"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux3"] = 3

  #Write the updated values in the file
  with open (cfg4, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE4 flex v2.4")
     json.dump(chip4, outfile, sort_keys = True, indent = 4) 
  f4_merge.close()

##################################################################################

def merge_2x1_v32(cfg1, cfg2, cfg3, cfg4):
  
  #Open the config file of FE1 in the module
  with open (cfg1) as f1_merge:
     chip1=json.load(f1_merge)
  #Access and change the value
  chip1["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias0"] = 500
  chip1["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip1["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip1["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip1["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip1["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 3
  chip1["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip1["RD53B"]["GlobalConfig"]["SerEnLane"]= 3
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 2
  chip1["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 3
  #Write the updated values in the file
  with open (cfg1, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE1 flex v3.2")
     json.dump(chip1, outfile, sort_keys = True, indent = 4) 
  f1_merge.close()

  #Open the config file of FE1 in the module
  with open (cfg2) as f2_merge:
     chip2=json.load(f2_merge)
  #Access and change the value
  chip2["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip2["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip2["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip2["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["CdrClkSel"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip2["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip2["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 1
  chip2["RD53B"]["GlobalConfig"]["SerEnLane"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 2
  chip2["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 3
  chip2["RD53B"]["GlobalConfig"]["DataMergeInMux0"] = 1
  chip2["RD53B"]["GlobalConfig"]["DataMergeInMux1"] = 0
  #Write the updated values in the file
  with open (cfg2, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE2 flex v3.2")
     json.dump(chip2, outfile, sort_keys = True, indent = 4) 
  f2_merge.close()

  #Open the config file of FE3 in the module
  with open (cfg3) as f3_merge:
     chip3=json.load(f3_merge)
  #Access and change the value
  chip3["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias0"] = 400
  chip3["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip3["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip3["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["CdrClkSel"] = 2
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip3["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip3["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 3
  chip3["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 0
  chip3["RD53B"]["GlobalConfig"]["SerEnLane"] = 3
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 2
  chip3["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 3
  #Write the updated values in the file
  with open (cfg3, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE3 flex v3.2")
     json.dump(chip3, outfile, sort_keys = True, indent = 4) 
  f3_merge.close()

  #Open the config file of FE4 in the module
  with open (cfg4) as f4_merge:
     chip4=json.load(f4_merge)
  #Access and change the value
  chip4["RD53B"]["GlobalConfig"]["SerEnTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["SerInvTap"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias0"] = 1000
  chip4["RD53B"]["GlobalConfig"]["CmlBias1"] = 0
  chip4["RD53B"]["GlobalConfig"]["CmlBias2"] = 0
  chip4["RD53B"]["GlobalConfig"]["ServiceBlockEn"] = 0
  chip4["RD53B"]["GlobalConfig"]["CdrClkSel"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnClkGate"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeSelClk"] = 0
  chip4["RD53B"]["GlobalConfig"]["EnChipId"] = 1
  chip4["RD53B"]["GlobalConfig"]["AuroraActiveLanes"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeEn"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeEnBond"] = 1
  chip4["RD53B"]["GlobalConfig"]["SerEnLane"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux0"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux1"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux2"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeOutMux3"] = 3
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux0"] = 1
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux1"] = 0
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux2"] = 2
  chip4["RD53B"]["GlobalConfig"]["DataMergeInMux3"] = 3

  #Write the updated values in the file
  with open (cfg4, 'w') as outfile:
     print("Dumping merging configuration 2 to 1 of FE4 flex v3.2")
     json.dump(chip4, outfile, sort_keys = True, indent = 4) 
  f4_merge.close()


##################################################################################

if __name__ == "__main__":
    import argparse
    import shutil

    parser=argparse.ArgumentParser("Generate FE config for data merging in 0.5 lanes/FE and 0.25 lanes/FE")
    parser.add_argument("--module_name", help = "Name of the module")
    parser.add_argument("--cnf_FE1", help = "Config file from module QC of FE1 in the module")
    parser.add_argument("--cnf_FE2", help = "Config file from module QC of FE2 in the module")
    parser.add_argument("--cnf_FE3", help = "Config file from module QC of FE3 in the module")
    parser.add_argument("--cnf_FE4", help = "Config file from module QC of FE4 in the module")
    parser.add_argument("--merge_type", help = "Chose between 2 if you are sending data in 2 links or 1 if you are sending data from all chips in one link"  )
    parser.add_argument("--flex_version", help = "Version of the flex design of the module. Choose between 2.4 and 3.2")
   
    args=parser.parse_args()

    #Names of the new config files
    #f1_merge = args.module_name + "_FE1_merge_"+ args.flex_version + "_" + args.merge_type + "to1.json"
    #f2_merge = args.module_name + "_FE2_merge_"+ args.flex_version + "_" + args.merge_type + "to1.json"
    #f3_merge = args.module_name + "_FE3_merge_"+ args.flex_version + "_" + args.merge_type + "to1.json"
    #f4_merge = args.module_name + "_FE4_merge_"+ args.flex_version + "_" + args.merge_type + "to1.json"
    f1_merge = args.module_name + "_FE1_merge_"+ args.merge_type + "to1.json"
    f2_merge = args.module_name + "_FE2_merge_"+ args.merge_type + "to1.json"
    f3_merge = args.module_name + "_FE3_merge_"+ args.merge_type + "to1.json"
    f4_merge = args.module_name + "_FE4_merge_"+ args.merge_type + "to1.json"
    print(f1_merge)
    print(type(f1_merge))

    #Copy the json files in the new merged ones
    shutil.copyfile(args.cnf_FE1,f1_merge)
    shutil.copyfile(args.cnf_FE2,f2_merge)
    shutil.copyfile(args.cnf_FE3,f3_merge)
    shutil.copyfile(args.cnf_FE4,f4_merge)
    
    #Create new merged files
    if (args.merge_type == "4" and args.flex_version == "2.4"):
        print("I am calling the 4 to 1 merging function for the flex version 2.4")
        merge_4x1_v24(f1_merge, f2_merge, f3_merge, f4_merge)
    elif (args.merge_type == "2" and args.flex_version == "2.4"):
        print("I am calling the 2 to 1 merging function for the flex version 2.4")
        merge_2x1_v24(f1_merge, f2_merge, f3_merge, f4_merge)
    elif (args.merge_type == "4" and args.flex_version == "3.2"):
        print("I am calling the 4 to 1 merging function for the flex version 3.2")
        merge_4x1_v32(f1_merge, f2_merge, f3_merge, f4_merge)
    elif (args.merge_type == "2" and args.flex_version == "3.2"):
        print("I am calling the 2 to 1 merging function for the flex version 2.4")
        merge_2x1_v32(f1_merge, f2_merge, f3_merge, f4_merge)
    else:
        print("Invalid merging option!")
