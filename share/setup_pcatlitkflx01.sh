#!/bin/bash

export ITK_DATA_PATH=/home/${USER}/dataITkPix

export TDAQ_PARTITION=ItkPix
export TDAQ_DB_DATA=itk/partitions/ItkPix.data.xml
export TDAQ_DB=oksconfig:$TDAQ_DB_DATA
export TDAQ_DB_PATH=/home/shared/itkpix/databases:$ITK_INST_PATH/share/data:$TDAQ_DB_PATH
export TDAQ_IPC_INIT_REF=file:/home/shared/ipc/ipc_root.ref


