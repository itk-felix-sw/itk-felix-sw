#!/usr/bin/env python
######################################################################
# Compare occupancy maps between itk-felix-sw and yarr
#
# Example: python compare_occ_maps.py 005357_lin_analogscan 002689_AnalogScanLin --chip_name CERNQ10_Chip2 
#
# @author Carlos.Solans@cern.ch
# @author Leyre.Flores.Sanz.De.Acedo@cern.ch
# @date October 2022
#######################################################################
import os
import sys
import math
import json
import ROOT

mem=[]

YARR_data_path = "/home/itkpix/YARRs/YARR_FELIX_MASTER/data/"
ITK_flx_sw_path = "/home/itkpix/data"

##Create a dictionary with the names of the Demonstrator M6PP0
M6PP0_demonstrator = {'CERNQ10_Chip2': ['M1_CERNQ10_Chip2','20UPGFC0013668'], 'CERNQ10_Chip4':['M1_CERNQ10_Chip4', '20UPGFC0013716'], 'Paris10_Chip2':['M2_Paris10_Chip2', '20UPGFC0008771'], 'Siegen1_Chip2':['M4_Siegen1_Chip2', '20UPGFC0012405'], 'Siegen1_Chip4':['M4_Siegen1_Chip4', '20UPGFC0012420'], 'Paris12_Chip2':['M5_Paris12_Chip2', '20UPGFC0012566'],'Siegen2_Chip2':['M6_Siegen2_Chip2', 'Siegen2_Chip_2'], 'Siegen2_Chip4':['M6_Siegen2_Chip4', 'Siegen2_Chip_4']}

##Definitions of the FE start and stop columns
SYN_COL0  = 0
SYN_COLN  = 128
LIN_COL0  = 128
LIN_COLN  = 264
DIFF_COL0 = 264
DIFF_COLN = 400
num_syn_pixels  = 24576
num_lin_pixels  = 25920
num_diff_pixels = 25920 

def parse(path,name):
    histo=None
    ext=os.path.splitext(path)[1]
    if "dat" in ext:
        cc=open(path).readlines()
        if "Histo1d" in cc[0]: 
            histo=parseHisto1d(cc)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in cc[0]:
            print("Parsing dat file: %s" % path)
            histo=parseHisto2d(cc)
            pass
        pass
    elif "json" in ext:
        try: j=json.load(open(path))
        except: return histo
        if "Histo1d" in j["Type"]:
            histo=jsonHisto1d(j,name)
            histo.SetLineColor(ROOT.kRed)
            histo.SetMarkerColor(ROOT.kRed)
            pass
        elif "Histo2d" in j["Type"]:
            print("Parsing json file: %s" % path)
            histo=jsonHisto2d(j,name)
            pass
        pass
    elif "root" in ext:
        print("Parsing root file: %s" % path)
        tf=ROOT.TFile(path)
        mem.append(tf)
        histo=tf.Get(name)
        histo.SetTitle("itk-felix-sw")
        histo.SetLineColor(ROOT.kBlue)
        histo.SetMarkerColor(ROOT.kBlue)
        pass
    return histo

def parseHisto2d(lines):
    if "Histo2d" not in lines[0]: return None
    name = lines[1].strip()
    title = "YARR;%s;%s;%s" % (lines[2].strip(),lines[3].strip(),lines[4].strip())
    xbins = int(lines[5].split()[0])
    xbin0 = float(lines[5].split()[1])
    xbinN = float(lines[5].split()[2])
    ybins = int(lines[6].split()[0])
    ybin0 = float(lines[6].split()[1])
    ybinN = float(lines[6].split()[2])
    histo = ROOT.TH2D(name,title,xbins,xbin0,xbinN,ybins,ybin0,ybinN)
    histo.Sumw2()
    for i in range(0,ybins):
        sdata = lines[8+i].strip().split()
        y=i+1
        x=0
        for sv in sdata:
            x+=1
            histo.SetBinContent(x,y,float(sv))
            histo.SetBinError(x,y,math.sqrt(float(sv)))
            pass
        pass
    return histo

if __name__ == "__main__":
    import argparse

    parser=argparse.ArgumentParser("Compare YARR and ITK-FELIX-SW plots")
    parser.add_argument("file1", help = "Run folder of YARR data")
    parser.add_argument("file2", help = "Run folder from itk-felix-sw")
    parser.add_argument("--chip_name", help = "name of the chip to be compared")
    parser.add_argument("--fe", help = "Front-end flavour of the data")
  
    args=parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gROOT.ForceStyle()    
    ROOT.gStyle.SetPadRightMargin(0.2)

    if args.file1:
    
        YARR_analysis_path = YARR_data_path +  args.file1
        print ("I am looking in the following folder: %s" %YARR_analysis_path )     
        if args.chip_name:
            dat_file = M6PP0_demonstrator[args.chip_name][1]+'_OccupancyMap.dat' 
            YARR_file = YARR_analysis_path + "/" + dat_file
            print ("The file to analyse is: %s" %YARR_file)

    if args.file2: 
        itk_flx_sw_path = ITK_flx_sw_path + "/" + args.file2 + "/output.root"  
        print ("I am looking in the following file: %s" %itk_flx_sw_path )     
        if args.chip_name:
            root_file = "occ_M6PP0_" + M6PP0_demonstrator[args.chip_name][0] 
            print ("I am looking at the following tree: %s" %root_file)
 
    if args.fe == "syn":
        start_bin_x = SYN_COL0
        stop_bin_x  = SYN_COLN
    elif args.fe == "lin": 
        start_bin_x = LIN_COL0
        stop_bin_x  = LIN_COLN
    elif args.fe == "diff": 
        start_bin_x = DIFF_COL0
        stop_bin_x  = DIFF_COLN
    elif args.fe == "lindiff": 
        start_bin_x = LIN_COL0
        stop_bin_x  = DIFF_COLN
    else:
        start_bin_x = 0
        stop_bin_x = 400

 
    histo1=parse(YARR_file, args.chip_name)
    histo2=parse(itk_flx_sw_path,root_file)
    print("Total number of X bins with YARR is %d" %histo1.GetNbinsX()) 
    print("Total number of X bins with ITK-flx-sw is %d" %histo2.GetNbinsX()) 
    print("Minimum value of X bin is %d and maximum is %d in YARR" %(histo1.GetXaxis().GetXmin(), histo1.GetXaxis().GetXmax())) 
    print("Minimum value of X bin is %d and maximum is %d in ITK-flx-sw" %(histo2.GetXaxis().GetXmin(), histo2.GetXaxis().GetXmax()))

    #Generating the histo that is the substraction of the other two
    histo_sub = histo2.Clone()
    #-1 is the factor histo 1 is multiplied by
    histo_sub.Add(histo1, -1)

    #Loop over the substraction to compute noise pixels/death pixels/perfect pixels
    Analysis_histo_YARR = ROOT.TH1D("hits", "; ; Pixels",5,-0.5,4.5)
    Analysis_histo_itk  = ROOT.TH1D("hits", "; ; Pixels",5,-0.5,4.5)
    Analysis_histo_itk_sub_YARR  = ROOT.TH1D("hits","; ; Pixels(ITK-flx-sw - YARR)",5,-0.5,4.5)

    #Definition of analysis variables
    noisy_pixel_YARR = 0
    death_pixel_YARR = 0
    perfect_occ_YARR = 0
    neither_nor_YARR = 0
    inneficient_YARR = 0 #Less than 95%
    good_enough_YARR = 0 #Betweem 95%-105%
    mask_pixels_YARR =[]

    for y in range(1, histo1.GetNbinsY()):
        for x in range(start_bin_x + 1, stop_bin_x):
            z = histo1.GetBinContent(x, y)
            if (z == 100):
                perfect_occ_YARR = perfect_occ_YARR + 1
            elif (z == 0):     
                death_pixel_YARR = death_pixel_YARR + 1
                mask_pixels_YARR.append(x)
                mask_pixels_YARR.append(y)
            elif (z > 105):
                noisy_pixel_YARR = noisy_pixel_YARR + 1
                mask_pixels_YARR.append(x)
                mask_pixels_YARR.append(y)
            elif (z>=95 and z<100):
                good_enough_YARR = good_enough_YARR + 1
            elif (z <95 and z > 0):
                 inneficient_YARR = inneficient_YARR + 1
            else:
                neither_nor_YARR = neither_nor_YARR + 1

    print("Total number of noisy pixels with YARR is: %d" %noisy_pixel_YARR)
    print("Total number of perfect pixels with YARR is: %d" %perfect_occ_YARR)
    print("Total number of not good nor too bad pixels with YARR is: %d" %neither_nor_YARR)
    print("Total number of inneficient pixels with YARR is: %d" %inneficient_YARR)
    print("Total number of good enough pixels with YARR is: %d" %good_enough_YARR)
    print("Total number of death pixels with YARR is: %d" %death_pixel_YARR)

    print(mask_pixels_YARR)

    #Filling the analysis histogram
    Analysis_histo_YARR.GetXaxis().SetBinLabel(1, "perfect_occ")
    Analysis_histo_YARR.SetBinContent(1, perfect_occ_YARR)
    Analysis_histo_YARR.GetXaxis().SetBinLabel(2, "noisy")
    Analysis_histo_YARR.SetBinContent(2, noisy_pixel_YARR)
    Analysis_histo_YARR.GetXaxis().SetBinLabel(3, "inneficient")
    Analysis_histo_YARR.SetBinContent(3, inneficient_YARR)
    Analysis_histo_YARR.GetXaxis().SetBinLabel(4, "good enough")
    Analysis_histo_YARR.SetBinContent(4, good_enough_YARR)
    Analysis_histo_YARR.GetXaxis().SetBinLabel(5, "death")
    Analysis_histo_YARR.SetBinContent(5, death_pixel_YARR)

    #Definition of analysis variables
    noisy_pixel_itk = 0
    death_pixel_itk = 0
    perfect_occ_itk = 0
    neither_nor_itk = 0
    inneficient_itk = 0 
    good_enough_itk = 0 
    mask_pixels_itk =[]

    for y in range(1 , histo2.GetNbinsY()):
        for x in range(start_bin_x + 1, stop_bin_x):
            z = histo2.GetBinContent(x, y)
            if (z == 100):
                perfect_occ_itk = perfect_occ_itk + 1
            elif (z == 0):     
                death_pixel_itk = death_pixel_itk + 1
                mask_pixels_itk.append(x)
                mask_pixels_itk.append(y)
            elif (z > 100):
                noisy_pixel_itk = noisy_pixel_itk + 1
                mask_pixels_itk.append(x)
                mask_pixels_itk.append(y)
            elif (z>=95 and z<100):
                good_enough_itk = good_enough_itk + 1
            elif (z <95 and z > 0):
                 inneficient_itk = inneficient_itk + 1
            else:
                neither_nor_itk = neither_nor_itk + 1

    print("Total number of noisy pixels with itk-flx-sw is: %d" %noisy_pixel_itk)
    print("Total number of perfect pixels with itk-flx-sw is: %d" %perfect_occ_itk)
    print("Total number of not good nor too bad pixels with itk-flx-sw is: %d" %neither_nor_itk)
    print("Total number of inneficient pixels with itk-flx-sw is: %d" %inneficient_itk)
    print("Total number of good enough pixels with itk-flx-sw is: %d" %good_enough_itk)
    print("Total number of death pixels with itk-flx-sw is: %d" %death_pixel_itk)
    print(mask_pixels_itk)

    #Filling the analysis histogram
    Analysis_histo_itk.GetXaxis().SetBinLabel(1, "perfect_occ")
    Analysis_histo_itk.Fill(1, perfect_occ_itk)
    Analysis_histo_itk.GetXaxis().SetBinLabel(2, "noisy")
    Analysis_histo_itk.Fill(2, noisy_pixel_itk)
    Analysis_histo_YARR.GetXaxis().SetBinLabel(3, "inneficient")
    Analysis_histo_YARR.SetBinContent(3, inneficient_itk)
    Analysis_histo_YARR.GetXaxis().SetBinLabel(4, "good enough")
    Analysis_histo_YARR.SetBinContent(4, good_enough_itk)
    Analysis_histo_itk.GetXaxis().SetBinLabel(5, "death")
    Analysis_histo_itk.Fill(5, death_pixel_itk)

    #Filling the analysis histogram
    Analysis_histo_itk_sub_YARR.GetXaxis().SetBinLabel(1, "perfect_occ")
    Analysis_histo_itk_sub_YARR.SetBinContent(1, (perfect_occ_itk - perfect_occ_YARR))
    Analysis_histo_itk_sub_YARR.GetXaxis().SetBinLabel(2, "noisy")
    Analysis_histo_itk_sub_YARR.SetBinContent(2, (noisy_pixel_itk - noisy_pixel_YARR))
    Analysis_histo_itk_sub_YARR.GetXaxis().SetBinLabel(3, "inneficient")
    Analysis_histo_itk_sub_YARR.SetBinContent(3, (inneficient_itk - inneficient_YARR))
    Analysis_histo_itk_sub_YARR.GetXaxis().SetBinLabel(4, "good_enough")
    Analysis_histo_itk_sub_YARR.SetBinContent(4, (good_enough_itk - good_enough_YARR))
    Analysis_histo_itk_sub_YARR.GetXaxis().SetBinLabel(5, "death")
    Analysis_histo_itk_sub_YARR.SetBinContent(4, (death_pixel_itk - death_pixel_YARR))

    #histo1=parse("/home/itkpix/YARRs/YARR_FELIX_MASTER/data/005049_diff_analogscan/Siegen2_Chip_2_OccupancyMap.dat", "Siegen2_Chip_2_OccupancyMap")
    #histo2=parse("/home/itkpix/data/002700_AnalogScanLinDiff/output.root", "occ_M6PP0_M6_Siegen2_Chip2")

    print(type(str(args.fe)))
    print(type(args.chip_name))

    c1=ROOT.TCanvas("c1","%s vs %s" % (histo1.GetTitle(), histo2.GetTitle()),1200,600)
    c1.Divide(2,1)
    c1.cd(1)
    histo1.Draw("colz")
    c1.cd(2)
    histo2.Draw("colz")
    c1.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/diff_lin" + str(args.fe) + args.chip_name + "_occupancy_map.pdf")
    #os.system("evince CERNQ10_occupancy_map.pdf &")
    
    c2=ROOT.TCanvas("c2", "Difference in occupancy", 0,0,1600, 800)
    ROOT.gPad.SetRightMargin(0.2)
    histo_sub.SetMaximum(5)
    histo_sub.SetMinimum(-5)
    histo_sub.Draw("colz")
    c2.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/diff_lin" + str(args.fe) + args.chip_name + "_difference_occ_YARR_vs_itk-flx-sw.pdf")
    #os.system("evince Difference_occ_YARR_vs_itk-flx-sw.pdf &")

    c=ROOT.TCanvas("canvas", "Analysis histo", 1200, 600)
    #ROOT.gStyle.SetEndErrorSize(3)
    #ROOT.gStyle.SetErrorX(1.)
    Analysis_histo_itk_sub_YARR.SetMarkerStyle(20)
    #Analysis_histo_itk_sub_YARR.SetMarkerStyle(24)
    Analysis_histo_itk_sub_YARR.Draw("HIST")
    #Analysis_histo_itk.Draw("ESAME")
    #Analysis_histo_YARR.SetFillColor(49)
    #Analysis_histo_itk.SetFillColor(38)
    legend=ROOT.TLegend(0.55,0.65,0.76,0.82)
    #legend.AddEntry(Analysis_histo_YARR, "ITK_FELIX_SW - YARR", "f") 
    #legend.AddEntry(Analysis_histo_YARR, "YARR", "f")    
    #legend.AddEntry(Analysis_histo_itk, "ITk-FLX_SW", "f")    
    #legend.Draw()
    c.SaveAs("/home/itkpix/data/M6PP0_occ_comparisons/Temp-10deg/diff_lin" + str(args.fe) + args.chip_name + "_analysis_occupancy_YARR_vs_ITk-flx-sw.pdf")
