import json
import argparse

def compare_json_files(file1, file2):
    with open(file1, 'r') as f1:
        data1 = json.load(f1)
    with open(file2, 'r') as f2:
        data2 = json.load(f2)

    differing_values = {}
    differing_registers_file1 = []
    differing_registers_file2 = []

    for register_name in data2["RD53B"]["GlobalConfig"]:
        if register_name not in data1["RD53B"]["GlobalConfig"]:
            differing_registers_file1.append(register_name)
        else: 
            if data1["RD53B"]["GlobalConfig"][register_name] != data2["RD53B"]["GlobalConfig"][register_name]:
                differing_values[register_name] = {
                    'file1_value': data1["RD53B"]["GlobalConfig"][register_name],
                    'file2_value': data2["RD53B"]["GlobalConfig"][register_name]
                }
    
    for register_name in data1["RD53B"]["GlobalConfig"]:
        if register_name not in data2["RD53B"]["GlobalConfig"]:
            differing_registers_file2.append(register_name)
        else: 
            if data1["RD53B"]["GlobalConfig"][register_name] != data2["RD53B"]["GlobalConfig"][register_name]:
                differing_values[register_name] = {
                    'file1_value': data1["RD53B"]["GlobalConfig"][register_name],
                    'file2_value': data2["RD53B"]["GlobalConfig"][register_name]
                }
    

    result = {
        'differing_values': differing_values,
        'Registers in file 1 but not in file 2': differing_registers_file2, 
        'Registers in file 2 but not in file 1': differing_registers_file1
    }

    with open('differences.json', 'w') as output_file:
        json.dump(result, output_file, indent=4)


if __name__ == '__main__':
    parser=argparse.ArgumentParser("Routine that check the differences in the global configuration registers of two chips and saves them in a file called differences.json")
    parser.add_argument("--cnf_chip1", help = "Chip 1 config file")
    parser.add_argument("--cnf_chip2", help = "Chip 2 config file")
    args=parser.parse_args()

    file1_path = args.cnf_chip1
    file2_path = args.cnf_chip2

    compare_json_files(file1_path, file2_path)
