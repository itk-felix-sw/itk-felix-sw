#!/usr/bin/env python
##################################
# Compress json config files
#
# Carlos.Solans@cern.ch
# November 2018
##################################

import os
import sys
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('file',help='file to compress')
parser.add_argument('-v','--verbose',action='store_true',help='verbose')
parser.add_argument('-t','--test',action='store_true',help='verbose')
args = parser.parse_args()

n1=os.path.basename(args.file)
if args.verbose: print("Compressing %s" % args.file)

j1=json.load(open(args.file))

p1=args.file if not args.test else args.file+".test"

fw=open(p1,'w+')
fw.write(json.dumps(j1))
fw.close()
